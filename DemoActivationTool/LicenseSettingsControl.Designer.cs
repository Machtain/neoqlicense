﻿namespace NeoQLicense.Windows.Controls
{
    partial class LicenseSettingsControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            grpbxLicenseType = new GroupBox();
            rdoVolumeLicense = new RadioButton();
            rdoSingleLicense = new RadioButton();
            grpbxUniqueID = new GroupBox();
            txtUID = new TextBox();
            grpbxLicenseInfo = new GroupBox();
            pgLicenseSettings = new PropertyGrid();
            btnGenLicense = new Button();
            grpbxLicenseType.SuspendLayout();
            grpbxUniqueID.SuspendLayout();
            grpbxLicenseInfo.SuspendLayout();
            SuspendLayout();
            // 
            // grpbxLicenseType
            // 
            grpbxLicenseType.Controls.Add(rdoVolumeLicense);
            grpbxLicenseType.Controls.Add(rdoSingleLicense);
            grpbxLicenseType.Location = new Point(3, 3);
            grpbxLicenseType.Name = "grpbxLicenseType";
            grpbxLicenseType.Size = new Size(321, 54);
            grpbxLicenseType.TabIndex = 0;
            grpbxLicenseType.TabStop = false;
            grpbxLicenseType.Text = "License Type";
            // 
            // rdoVolumeLicense
            // 
            rdoVolumeLicense.Location = new Point(116, 22);
            rdoVolumeLicense.Name = "rdoVolumeLicense";
            rdoVolumeLicense.Size = new Size(107, 24);
            rdoVolumeLicense.TabIndex = 0;
            rdoVolumeLicense.Text = "Volume License";
            rdoVolumeLicense.UseVisualStyleBackColor = true;
            rdoVolumeLicense.CheckedChanged += LicenseTypeRadioButtons_CheckedChanged;
            // 
            // rdoSingleLicense
            // 
            rdoSingleLicense.Checked = true;
            rdoSingleLicense.Location = new Point(6, 22);
            rdoSingleLicense.Name = "rdoSingleLicense";
            rdoSingleLicense.Size = new Size(104, 24);
            rdoSingleLicense.TabIndex = 1;
            rdoSingleLicense.TabStop = true;
            rdoSingleLicense.Text = "Single License";
            rdoSingleLicense.UseVisualStyleBackColor = true;
            rdoSingleLicense.CheckedChanged += LicenseTypeRadioButtons_CheckedChanged;
            // 
            // grpbxUniqueID
            // 
            grpbxUniqueID.Controls.Add(txtUID);
            grpbxUniqueID.Location = new Point(3, 63);
            grpbxUniqueID.Name = "grpbxUniqueID";
            grpbxUniqueID.Size = new Size(321, 59);
            grpbxUniqueID.TabIndex = 1;
            grpbxUniqueID.TabStop = false;
            grpbxUniqueID.Text = "License UID";
            // 
            // txtUID
            // 
            txtUID.Location = new Point(6, 22);
            txtUID.Name = "txtUID";
            txtUID.Size = new Size(309, 23);
            txtUID.TabIndex = 0;
            // 
            // grpbxLicenseInfo
            // 
            grpbxLicenseInfo.Controls.Add(pgLicenseSettings);
            grpbxLicenseInfo.Location = new Point(3, 128);
            grpbxLicenseInfo.Name = "grpbxLicenseInfo";
            grpbxLicenseInfo.Size = new Size(321, 233);
            grpbxLicenseInfo.TabIndex = 2;
            grpbxLicenseInfo.TabStop = false;
            grpbxLicenseInfo.Text = "License Information";
            // 
            // pgLicenseSettings
            // 
            pgLicenseSettings.Location = new Point(6, 22);
            pgLicenseSettings.Name = "pgLicenseSettings";
            pgLicenseSettings.PropertySort = PropertySort.NoSort;
            pgLicenseSettings.Size = new Size(309, 205);
            pgLicenseSettings.TabIndex = 0;
            // 
            // btnGenLicense
            // 
            btnGenLicense.Location = new Point(185, 367);
            btnGenLicense.Name = "btnGenLicense";
            btnGenLicense.Size = new Size(133, 23);
            btnGenLicense.TabIndex = 3;
            btnGenLicense.Text = "Generate License";
            btnGenLicense.UseVisualStyleBackColor = true;
            btnGenLicense.Click += btnGenLicense_Click;
            // 
            // LicenseSettingsControl
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(grpbxLicenseType);
            Controls.Add(grpbxUniqueID);
            Controls.Add(grpbxLicenseInfo);
            Controls.Add(btnGenLicense);
            Name = "LicenseSettingsControl";
            Size = new Size(329, 395);
            grpbxLicenseType.ResumeLayout(false);
            grpbxUniqueID.ResumeLayout(false);
            grpbxUniqueID.PerformLayout();
            grpbxLicenseInfo.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.GroupBox grpbxLicenseType;
        private System.Windows.Forms.RadioButton rdoVolumeLicense;
        private System.Windows.Forms.RadioButton rdoSingleLicense;
        private System.Windows.Forms.GroupBox grpbxUniqueID;
        private System.Windows.Forms.TextBox txtUID;
        private System.Windows.Forms.GroupBox grpbxLicenseInfo;
        private System.Windows.Forms.PropertyGrid pgLicenseSettings;
        private System.Windows.Forms.Button btnGenLicense;


    }
}
