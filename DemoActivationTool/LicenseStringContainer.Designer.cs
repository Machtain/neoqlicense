﻿namespace NeoQLicense.Windows.Controls
{
    partial class LicenseStringContainer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            grpbxLicStr = new GroupBox();
            lnkSaveToFile = new LinkLabel();
            lnkCopy = new LinkLabel();
            dlgSaveFile = new SaveFileDialog();
            txtLicense = new RichTextBox();
            grpbxLicStr.SuspendLayout();
            SuspendLayout();
            // 
            // grpbxLicStr
            // 
            grpbxLicStr.Controls.Add(txtLicense);
            grpbxLicStr.Controls.Add(lnkSaveToFile);
            grpbxLicStr.Controls.Add(lnkCopy);
            grpbxLicStr.Location = new Point(3, 3);
            grpbxLicStr.Name = "grpbxLicStr";
            grpbxLicStr.Size = new Size(318, 404);
            grpbxLicStr.TabIndex = 0;
            grpbxLicStr.TabStop = false;
            grpbxLicStr.Text = "License String";
            // 
            // lnkSaveToFile
            // 
            lnkSaveToFile.LinkBehavior = LinkBehavior.HoverUnderline;
            lnkSaveToFile.Location = new Point(6, 385);
            lnkSaveToFile.Name = "lnkSaveToFile";
            lnkSaveToFile.Size = new Size(69, 16);
            lnkSaveToFile.TabIndex = 0;
            lnkSaveToFile.TabStop = true;
            lnkSaveToFile.Text = "Save to File";
            lnkSaveToFile.VisitedLinkColor = Color.Blue;
            lnkSaveToFile.LinkClicked += lnkSaveToFile_LinkClicked;
            // 
            // lnkCopy
            // 
            lnkCopy.LinkBehavior = LinkBehavior.HoverUnderline;
            lnkCopy.Location = new Point(208, 384);
            lnkCopy.Name = "lnkCopy";
            lnkCopy.Size = new Size(104, 17);
            lnkCopy.TabIndex = 1;
            lnkCopy.TabStop = true;
            lnkCopy.Text = "Copy to Clipboard";
            lnkCopy.VisitedLinkColor = Color.Blue;
            lnkCopy.LinkClicked += lnkCopy_LinkClicked;
            // 
            // dlgSaveFile
            // 
            dlgSaveFile.FileName = "License.lic";
            // 
            // txtLicense
            // 
            txtLicense.BackColor = SystemColors.Control;
            txtLicense.Location = new Point(6, 22);
            txtLicense.Name = "txtLicense";
            txtLicense.ReadOnly = true;
            txtLicense.Size = new Size(306, 343);
            txtLicense.TabIndex = 3;
            txtLicense.Text = "";
            // 
            // LicenseStringContainer
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(grpbxLicStr);
            Name = "LicenseStringContainer";
            Size = new Size(325, 410);
            grpbxLicStr.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.GroupBox grpbxLicStr;
        private System.Windows.Forms.LinkLabel lnkSaveToFile;
        private System.Windows.Forms.LinkLabel lnkCopy;
        private System.Windows.Forms.SaveFileDialog dlgSaveFile;
        private RichTextBox txtLicense;
    }
}
