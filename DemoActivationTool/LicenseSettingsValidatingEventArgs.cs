﻿using NeoQLicense;
using System;

namespace NeoQLicense.Windows.Controls
{
    public class LicenseSettingsValidatingEventArgs : EventArgs
    {
        public LicenseEntity License { get; set; }
        public bool CancelGenerating { get; set; }
    }
}
