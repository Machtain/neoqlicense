﻿namespace DemoActivationTool
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            dlgSaveFile = new SaveFileDialog();
            grpbxLicSettings = new GroupBox();
            licSettings = new NeoQLicense.Windows.Controls.LicenseSettingsControl();
            licString = new NeoQLicense.Windows.Controls.LicenseStringContainer();
            grpbxLicSettings.SuspendLayout();
            SuspendLayout();
            // 
            // dlgSaveFile
            // 
            dlgSaveFile.FileName = "License.lic";
            dlgSaveFile.Filter = "License File(*.lic)|*.lic";
            // 
            // grpbxLicSettings
            // 
            grpbxLicSettings.Controls.Add(licSettings);
            grpbxLicSettings.Location = new Point(5, 6);
            grpbxLicSettings.Margin = new Padding(4, 3, 4, 3);
            grpbxLicSettings.Name = "grpbxLicSettings";
            grpbxLicSettings.Padding = new Padding(4, 3, 4, 3);
            grpbxLicSettings.Size = new Size(400, 424);
            grpbxLicSettings.TabIndex = 6;
            grpbxLicSettings.TabStop = false;
            grpbxLicSettings.Text = "License Settings";
            // 
            // licSettings
            // 
            licSettings.AllowVolumeLicense = true;
            licSettings.Location = new Point(4, 20);
            licSettings.Margin = new Padding(4, 3, 4, 3);
            licSettings.Name = "licSettings";
            licSettings.Size = new Size(383, 395);
            licSettings.TabIndex = 7;
            licSettings.OnLicenseGenerated += licSettings_OnLicenseGenerated;
            // 
            // licString
            // 
            licString.LicenseString = "";
            licString.Location = new Point(410, 6);
            licString.Margin = new Padding(4, 3, 4, 3);
            licString.Name = "licString";
            licString.Size = new Size(325, 422);
            licString.TabIndex = 5;
            // 
            // frmMain
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(737, 433);
            Controls.Add(grpbxLicSettings);
            Controls.Add(licString);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Margin = new Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "frmMain";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Demo Activation Tool";
            Load += frmMain_Load;
            grpbxLicSettings.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.SaveFileDialog dlgSaveFile;
        private NeoQLicense.Windows.Controls.LicenseStringContainer licString;
        private System.Windows.Forms.GroupBox grpbxLicSettings;
        private NeoQLicense.Windows.Controls.LicenseSettingsControl licSettings;
    }
}

