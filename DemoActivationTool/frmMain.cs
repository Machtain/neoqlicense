﻿using System.Security;
using System.Reflection;
using NeoQLicense;
using DemoLicense;

// Neo QLicense

namespace DemoActivationTool
{
    public partial class frmMain : Form
    {
        private byte[] _certPubicKeyData;
        private SecureString _certPwd = new SecureString();

        public frmMain()
        {
            InitializeComponent();

            // PASSWORD OF .PFX
            _certPwd.AppendChar('M');
            _certPwd.AppendChar('a');
            _certPwd.AppendChar('c');
            _certPwd.AppendChar('h');
            _certPwd.AppendChar('t');
            _certPwd.AppendChar('a');
            _certPwd.AppendChar('i');
            _certPwd.AppendChar('n');
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //Read public key
            using (MemoryStream _mem = new MemoryStream())
            {
                using (var _file = new FileStream("perso.pfx", FileMode.Open))
                {
                    _file.CopyTo(_mem);

                    _certPubicKeyData = _mem.ToArray();
                }
            }

            //Initialize the path for the certificate to sign the XML license file
            licSettings.CertificatePrivateKeyData = _certPubicKeyData;
            licSettings.CertificatePassword = _certPwd;

            //Initialize a new license object
            licSettings.License = new MyLicense();
        }

        private void licSettings_OnLicenseGenerated(object sender, NeoQLicense.Windows.Controls.LicenseGeneratedEventArgs e)
        {
            //Event raised when license string is generated. Just show it in the text box
            licString.LicenseString = e.LicenseBASE64String;
        }


        private void btnGenSvrMgmLic_Click(object sender, EventArgs e)
        {
            //Event raised when "Generate License" button is clicked. 
            //Call the core library to generate the license
            licString.LicenseString = LicenseHandler.GenerateLicenseBASE64String(
                new MyLicense(),
                _certPubicKeyData,
                _certPwd);
        }

    }
}
