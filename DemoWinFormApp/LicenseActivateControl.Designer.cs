﻿namespace NeoQLicense.Windows.Controls
{
    partial class LicenseActivateControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            txtUID = new TextBox();
            lnkCopy = new LinkLabel();
            lblUIDTip = new Label();
            grpbxLicense = new GroupBox();
            grpbxUID = new GroupBox();
            txtLicense = new RichTextBox();
            grpbxLicense.SuspendLayout();
            grpbxUID.SuspendLayout();
            SuspendLayout();
            // 
            // txtUID
            // 
            txtUID.Location = new Point(6, 42);
            txtUID.Name = "txtUID";
            txtUID.ReadOnly = true;
            txtUID.Size = new Size(344, 23);
            txtUID.TabIndex = 2;
            // 
            // lnkCopy
            // 
            lnkCopy.LinkBehavior = LinkBehavior.HoverUnderline;
            lnkCopy.Location = new Point(245, 68);
            lnkCopy.Name = "lnkCopy";
            lnkCopy.Size = new Size(105, 23);
            lnkCopy.TabIndex = 1;
            lnkCopy.TabStop = true;
            lnkCopy.Text = "Copy to Clipboard";
            lnkCopy.VisitedLinkColor = Color.Blue;
            lnkCopy.LinkClicked += lnkCopy_LinkClicked;
            // 
            // lblUIDTip
            // 
            lblUIDTip.Location = new Point(6, 19);
            lblUIDTip.Name = "lblUIDTip";
            lblUIDTip.Size = new Size(344, 20);
            lblUIDTip.TabIndex = 0;
            lblUIDTip.Text = "Provide the below UID when purchasing the license";
            // 
            // grpbxLicense
            // 
            grpbxLicense.Controls.Add(txtLicense);
            grpbxLicense.Location = new Point(9, 106);
            grpbxLicense.Name = "grpbxLicense";
            grpbxLicense.Size = new Size(356, 195);
            grpbxLicense.TabIndex = 0;
            grpbxLicense.TabStop = false;
            grpbxLicense.Text = "License";
            // 
            // grpbxUID
            // 
            grpbxUID.Controls.Add(lblUIDTip);
            grpbxUID.Controls.Add(lnkCopy);
            grpbxUID.Controls.Add(txtUID);
            grpbxUID.Location = new Point(9, 3);
            grpbxUID.Name = "grpbxUID";
            grpbxUID.Size = new Size(356, 97);
            grpbxUID.TabIndex = 1;
            grpbxUID.TabStop = false;
            grpbxUID.Text = "License UID";
            // 
            // txtLicense
            // 
            txtLicense.BackColor = SystemColors.ControlLightLight;
            txtLicense.Location = new Point(6, 22);
            txtLicense.Name = "txtLicense";
            txtLicense.Size = new Size(344, 167);
            txtLicense.TabIndex = 4;
            txtLicense.Text = "";
            // 
            // LicenseActivateControl
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(grpbxLicense);
            Controls.Add(grpbxUID);
            Name = "LicenseActivateControl";
            Size = new Size(371, 305);
            grpbxLicense.ResumeLayout(false);
            grpbxUID.ResumeLayout(false);
            grpbxUID.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private TextBox txtUID;
        private LinkLabel lnkCopy;
        private Label lblUIDTip;
        private GroupBox grpbxLicense;
        private GroupBox grpbxUID;
        private RichTextBox txtLicense;
    }
}
