﻿namespace NeoQLicense.Windows.Controls
{
    partial class LicenseInfoControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            grpbxLicInfo = new GroupBox();
            txtLicInfo = new RichTextBox();
            grpbxLicInfo.SuspendLayout();
            SuspendLayout();
            // 
            // grpbxLicInfo
            // 
            grpbxLicInfo.Controls.Add(txtLicInfo);
            grpbxLicInfo.Location = new Point(3, 3);
            grpbxLicInfo.Name = "grpbxLicInfo";
            grpbxLicInfo.Size = new Size(212, 243);
            grpbxLicInfo.TabIndex = 0;
            grpbxLicInfo.TabStop = false;
            grpbxLicInfo.Text = "License Information";
            // 
            // txtLicInfo
            // 
            txtLicInfo.BackColor = SystemColors.Control;
            txtLicInfo.Location = new Point(6, 22);
            txtLicInfo.Name = "txtLicInfo";
            txtLicInfo.ReadOnly = true;
            txtLicInfo.Size = new Size(200, 215);
            txtLicInfo.TabIndex = 4;
            txtLicInfo.Text = "";
            // 
            // LicenseInfoControl
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(grpbxLicInfo);
            Name = "LicenseInfoControl";
            Size = new Size(218, 249);
            grpbxLicInfo.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.GroupBox grpbxLicInfo;
        private RichTextBox txtLicInfo;
    }
}
