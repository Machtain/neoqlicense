﻿using System.ComponentModel;

namespace DemoWinFormApp
{
    partial class frmActivation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            licActCtrl = new NeoQLicense.Windows.Controls.LicenseActivateControl();
            btnCancel = new Button();
            btnOK = new Button();
            SuspendLayout();
            // 
            // licActCtrl
            // 
            licActCtrl.AppName = null;
            licActCtrl.LicenseObjectType = null;
            licActCtrl.Location = new Point(14, 14);
            licActCtrl.Margin = new Padding(4, 3, 4, 3);
            licActCtrl.Name = "licActCtrl";
            licActCtrl.ShowMessageAfterValidation = true;
            licActCtrl.Size = new Size(374, 308);
            licActCtrl.TabIndex = 0;
            // 
            // btnCancel
            // 
            btnCancel.Location = new Point(290, 328);
            btnCancel.Margin = new Padding(4, 3, 4, 3);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(88, 27);
            btnCancel.TabIndex = 1;
            btnCancel.Text = "&Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // btnOK
            // 
            btnOK.Location = new Point(194, 328);
            btnOK.Margin = new Padding(4, 3, 4, 3);
            btnOK.Name = "btnOK";
            btnOK.Size = new Size(88, 27);
            btnOK.TabIndex = 2;
            btnOK.Text = "&OK";
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Click += btnOK_Click;
            // 
            // frmActivation
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(392, 368);
            Controls.Add(btnOK);
            Controls.Add(btnCancel);
            Controls.Add(licActCtrl);
            Margin = new Padding(4, 3, 4, 3);
            Name = "frmActivation";
            Text = "frmActivation";
            Load += frmActivation_Load;
            ResumeLayout(false);
        }

        #endregion

        private NeoQLicense.Windows.Controls.LicenseActivateControl licActCtrl;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
    }
}