﻿namespace DemoWinFormApp
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            licInfo = new NeoQLicense.Windows.Controls.LicenseInfoControl();
            SuspendLayout();
            // 
            // licInfo
            // 
            licInfo.DateFormat = null;
            licInfo.DateTimeFormat = null;
            licInfo.Location = new Point(14, 14);
            licInfo.Margin = new Padding(4, 3, 4, 3);
            licInfo.Name = "licInfo";
            licInfo.Size = new Size(220, 253);
            licInfo.TabIndex = 0;
            // 
            // frmMain
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(244, 273);
            Controls.Add(licInfo);
            Margin = new Padding(4, 3, 4, 3);
            Name = "frmMain";
            Text = "DemoWinFormApp";
            Load += frmMain_Load;
            Shown += frmMain_Shown;
            ResumeLayout(false);
        }

        #endregion

        private NeoQLicense.Windows.Controls.LicenseInfoControl licInfo;
    }
}

